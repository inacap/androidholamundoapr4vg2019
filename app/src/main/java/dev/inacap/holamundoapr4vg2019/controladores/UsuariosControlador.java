package dev.inacap.holamundoapr4vg2019.controladores;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import dev.inacap.holamundoapr4vg2019.modelos.BDPrincipalContrato;
import dev.inacap.holamundoapr4vg2019.modelos.UsuariosModelo;

public class UsuariosControlador {
    // Conectar a la capa modelo
    private UsuariosModelo capaModelo;

    public UsuariosControlador(Context context){
        this.capaModelo = new UsuariosModelo(context);
    }

    public void crearUsuario(String nombre, String pass){
        // Procesar los datos para que despues sean guardados
        ContentValues datosUsuario = new ContentValues();
        datosUsuario.put(BDPrincipalContrato.BDPrincipalUsuarios.COLUMNA_USERNAME, nombre);
        datosUsuario.put(BDPrincipalContrato.BDPrincipalUsuarios.COLUMNA_PASSWORD, pass);

        this.capaModelo.crearUsuario(datosUsuario);
    }


    public boolean procesarLogin(String username, String password){
        ContentValues usuario = this.capaModelo.obtenerUsuario(username);
        Log.i("PRUEBA", "Usuario: " + usuario.toString());

        if(usuario == null) return false;

        if(usuario.get(BDPrincipalContrato.BDPrincipalUsuarios.COLUMNA_USERNAME).equals(username) &&
                usuario.get(BDPrincipalContrato.BDPrincipalUsuarios.COLUMNA_PASSWORD).equals(password)){
            return true;
        }

        return false;
    }
}
