package dev.inacap.holamundoapr4vg2019.vistas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import dev.inacap.holamundoapr4vg2019.R;
import dev.inacap.holamundoapr4vg2019.controladores.UsuariosControlador;

public class CrearCuentaActivity extends AppCompatActivity {

    private EditText etNombre, etPass1, etPass2;
    private Button btRegistrar;
    private TextView tvCancelar;

    private UsuariosControlador capaControlador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_cuenta);
        this.capaControlador = new UsuariosControlador(getApplicationContext());

        this.etNombre = findViewById(R.id.etNombre);
        this.etPass1 = findViewById(R.id.etPassword1);
        this.etPass2 = findViewById(R.id.etPassword2);
        this.btRegistrar = findViewById(R.id.btRegistrar);
        this.tvCancelar = findViewById(R.id.tvCancelar);

        this.tvCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        this.btRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombre = etNombre.getText().toString().trim();
                String pass1 = etPass1.getText().toString().trim();
                String pass2 = etPass2.getText().toString().trim();

                if(!pass1.equals("") && pass1.equals(pass2)){
                    capaControlador.crearUsuario(nombre, pass1);
                    Toast.makeText(getApplicationContext(), "Cuenta creada", Toast.LENGTH_SHORT).show();
                    finish();
                } else Toast.makeText(getApplicationContext(), "Revisar contraseñas", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
