package dev.inacap.holamundoapr4vg2019.modelos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BDPrincipalHelper extends SQLiteOpenHelper {

    public static final String NOMBRE_BD = "BDPrincipal.db";
    public static final int VERSION_BD = 1;

    public static final String SQL_TABLA_USUARIOS = String.format(
            "CREATE TABLE %s (%s INTEGER PRIMARY KEY, %s TEXT, %s TEXT)",
            BDPrincipalContrato.BDPrincipalUsuarios.NOMBRE_TABLA,
            BDPrincipalContrato.BDPrincipalUsuarios.COLUMNA_ID,
            BDPrincipalContrato.BDPrincipalUsuarios.COLUMNA_USERNAME,
            BDPrincipalContrato.BDPrincipalUsuarios.COLUMNA_PASSWORD
    );

    public BDPrincipalHelper(Context context){
        super(context, NOMBRE_BD, null, VERSION_BD);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_TABLA_USUARIOS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //TODO: Debemos programar la actualizacion
    }
}
