package dev.inacap.holamundoapr4vg2019.modelos;

public class BDPrincipalContrato {

    // Asi impedimos la creacion de objetos de esta clase
    private BDPrincipalContrato(){}

    // Nos saltamos la instanciacion BDPrincipalContrato bdCOntrato = new BDPrincipalContrato();
    // BDPrincpalContrato.BDPrincipalUsarios

    public static class BDPrincipalUsuarios {
        public static final String NOMBRE_TABLA = "usuarios";
        public static final String COLUMNA_ID = "id";
        public static final String COLUMNA_USERNAME = "username";
        public static final String COLUMNA_PASSWORD = "password";
    }
}
