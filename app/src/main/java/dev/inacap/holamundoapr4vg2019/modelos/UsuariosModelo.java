package dev.inacap.holamundoapr4vg2019.modelos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class UsuariosModelo {
    private BDPrincipalHelper bdHelper;

    public UsuariosModelo(Context context){
        this.bdHelper = new BDPrincipalHelper(context);
    }

    public void crearUsuario(ContentValues usuario){
        // Abrir la base de datos
        SQLiteDatabase baseDatos = this.bdHelper.getWritableDatabase();
        baseDatos.insert(BDPrincipalContrato.BDPrincipalUsuarios.NOMBRE_TABLA, null, usuario);
    }


    public ContentValues obtenerUsuario(String username){
        // La consulta basica
        String consulta = String.format("SELECT * FROM usuarios WHERE %s = ?", BDPrincipalContrato.BDPrincipalUsuarios.COLUMNA_USERNAME);

        // El nombre de la columna, y el valor que queremos buscar
        String[] parametros = new String[]{
                username
        };

        // Abrimos la base de datos en modo lectura
        SQLiteDatabase db = this.bdHelper.getReadableDatabase();

        // Pedimos los datos
        Cursor cursor = db.rawQuery(consulta, parametros);

        // Nos movemos a la primera fila de los datos
        if(cursor.moveToFirst()){
            // Si entramos es por que hay datos

            // Sacar los datos del cursor
            String cursor_username = cursor.getString(cursor.getColumnIndex(BDPrincipalContrato.BDPrincipalUsuarios.COLUMNA_USERNAME));
            String cursor_password = cursor.getString(cursor.getColumnIndex(BDPrincipalContrato.BDPrincipalUsuarios.COLUMNA_PASSWORD));

            ContentValues usuario = new ContentValues();
            usuario.put(BDPrincipalContrato.BDPrincipalUsuarios.COLUMNA_USERNAME, cursor_username);
            usuario.put(BDPrincipalContrato.BDPrincipalUsuarios.COLUMNA_PASSWORD, cursor_password);

            return usuario;
        }


        return null;
    }
}
